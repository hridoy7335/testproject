﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace TestCoreForScaffolding.Models
{
    public partial class TestDBContext : DbContext
    {
        public TestDBContext()
        {
        }

        public TestDBContext(DbContextOptions<TestDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ClassTbl> ClassTbl { get; set; }
        public virtual DbSet<CourseTbl> CourseTbl { get; set; }
        public virtual DbSet<StudentTbl> StudentTbl { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=.\\SQLExpress;Database=TestDB;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ClassTbl>(entity =>
            {
                entity.HasKey(e => e.ClId);

                entity.Property(e => e.ClId).HasColumnName("cl_id");

                entity.Property(e => e.CId).HasColumnName("c_id");

                entity.Property(e => e.ClTime).HasColumnName("cl_time");

                entity.Property(e => e.SId).HasColumnName("s_id");
            });

            modelBuilder.Entity<CourseTbl>(entity =>
            {
                entity.HasKey(e => e.CId);

                entity.Property(e => e.CId).HasColumnName("c_id");

                entity.Property(e => e.CCredit)
                    .IsRequired()
                    .HasColumnName("c_credit")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CName)
                    .IsRequired()
                    .HasColumnName("c_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<StudentTbl>(entity =>
            {
                entity.HasKey(e => e.SId);

                entity.Property(e => e.SId).HasColumnName("s_id");

                entity.Property(e => e.SContact)
                    .IsRequired()
                    .HasColumnName("s_contact")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SName)
                    .IsRequired()
                    .HasColumnName("s_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
