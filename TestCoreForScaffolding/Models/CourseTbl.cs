﻿using System;
using System.Collections.Generic;

namespace TestCoreForScaffolding.Models
{
    public partial class CourseTbl
    {
        public int CId { get; set; }
        public string CName { get; set; }
        public string CCredit { get; set; }
    }
}
