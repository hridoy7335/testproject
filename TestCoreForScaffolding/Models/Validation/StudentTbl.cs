﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TestCoreForScaffolding.Models
{
   // [MetadataType((typeof(MetadataStudentTbl)))]
    public partial class StudentTbl
    {
    }

    public class MetadataStudentTbl
    {
        [Display(Name ="Student Name: ")]
        public string SName { get; set; }

        [Display(Name ="Student Contact")]
        public string SContact { get; set; }
    }
}
