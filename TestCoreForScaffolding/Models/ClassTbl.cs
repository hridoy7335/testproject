﻿using System;
using System.Collections.Generic;

namespace TestCoreForScaffolding.Models
{
    public partial class ClassTbl
    {
        public int ClId { get; set; }
        public TimeSpan ClTime { get; set; }
        public int SId { get; set; }
        public int CId { get; set; }
    }
}
