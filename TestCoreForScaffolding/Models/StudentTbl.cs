﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TestCoreForScaffolding.Models
{
    [MetadataType((typeof(MetadataStudentTbl)))]
    public partial class StudentTbl
    {
        public int SId { get; set; }
        
        [Display(Name ="Student Name")]
        public string SName { get; set; }
        public string SContact { get; set; }
    }
}
