﻿

//var columnDefs = [
//    { headerName: "Make", field: "make", sortable: true, filter: true },
//    { headerName: "Model", field: "model", sortable: true, filter: true },
//    { headerName: "Price", field: "price", sortable: true, filter: true },
//    { headerName: "Name", field: "Name", sortable: true, filter: true },
//    { headerName: "Age", field: "Age", sortable: true, filtre: true }
//];

//// specify the data
//var rowData = [
//    { make: "Toyota", model: "Celica", price: 35000, Name:"Aroni",Age: 20},
//    { make: "Ford", model: "Mondeo", price: 32000, Name: "BMoni", Age: 22},
//    { make: "Porsche", model: "Boxter", price: 72000, Name: "CGoni", Age: 10}
//];

//// let the grid know which columns and what data to use
//var gridOptions = {
//    columnDefs: columnDefs,
//    rowData: rowData
//};

//// setup the grid after the page has finished loading
//document.addEventListener('DOMContentLoaded', function () {
//    var gridDiv = document.querySelector('#myGrid');
//    new agGrid.Grid(gridDiv, gridOptions);
//});


// data get from API

//var columnDefs = [
//    { headerName: "Make", field: "make", filter: true, sortable: true, checkboxSelection: true, rowGroup: true },
//    { headerName: "Model", field: "model", filter: true, sortable: true, checkboxSelection: true, rowGroup: true },
//    { headerName: "Price", field: "price", filter: true, sortable: true }
//];

var columnDefs = [
    { headerName: "Make", field: "make", rowGroup: true },
    { headerName: "Model", field: "model" },
    { headerName: "Price", field: "price" }
];



var autoGroupColumnDef = {
    headerName: "Model",
    field: "model",
    cellRenderer: 'agGroupCellRenderer',
    cellRendererParams: {
        checkbox: true
    }
}

var gridOptions = {
    columnDefs: columnDefs,
    autoGroupColumnDef: autoGroupColumnDef,
    groupSelectsChildren: true,
    rowSelection: 'multiple'

};

  // setup the grid after the page has finished loading
    document.addEventListener('DOMContentLoaded', function () {
        var gridDiv = document.querySelector('#myGrid');
        new agGrid.Grid(gridDiv, gridOptions);
        agGrid.simpleHttpRequest({ url: 'https://raw.githubusercontent.com/ag-grid/ag-grid/master/grid-packages/ag-grid-docs/src/sample-data/rowData.json' }).then(function (data) {
            gridOptions.api.setRowData(data);
        });
    })

function getSelectionRow() {
    var selectedNodes = gridOptions.api.getSelectedNodes()
    var selectedData = selectedNodes.map(function (node) { return node.data })
    var selectedDataStringPresentation = selectedData.map(function (node) { return node.make + ' ' + node.model }).join(', ')
    alert('Selected nodes: ' + selectedDataStringPresentation);
}

