﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestCoreForScaffolding.Models;

namespace TestCoreForScaffolding.Controllers
{
    public class Test : Controller
    {
        Models.TestDBContext db = new TestDBContext();

        public IActionResult Index()
        {
            return View();
        }

        public ActionResult StudentList()
        {
            return View(db.StudentTbl.ToList());
        }
    }
}
